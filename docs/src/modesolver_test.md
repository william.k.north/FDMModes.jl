# modesolver.jl

## Description

We test the 1D (uniform grid) FDM implementation's values for the modal
effective indices against some values given in literature, namely
those in Appendix 17 of Coldren's book.

We also test the 2D (uniform and non-uniform) grid FDM implementation's
values for the modal effective indices against results obtained
from the `svmodes` function from
[WGMODES](https://photonics.umd.edu/software/wgmodes/)
(
[GitLab mirror](https://gitlab.com/pawelstrzebonski/WGMODES)
and Julia port
[WGMODES.jl](https://gitlab.com/pawelstrzebonski/WGMODES.jl)
).

We also test the modal effective index values for approximate consistency
between uniform and non-uniform sampling of the same waveguide.
