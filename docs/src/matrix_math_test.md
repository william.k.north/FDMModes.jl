# matrix_math.jl

## Description

The `LinearAlgebra.kron` and other `LinearAlgebra` functions will
produce dense matrices for certain combinations of input matrices. We
should (almost?) never actually be using dense matrices so we test
which combination of matrix/array types and functions preserve sparsity
and/or "special" matrix type as to ensure we do not accidentally produce
massive dense matrices.
