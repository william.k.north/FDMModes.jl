# utils.jl

## Description

Misc utility functions for the rest of the package.

## Functions

```@autodocs
Modules = [FDMModes]
Pages   = ["utils.jl"]
```
