# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Optimizations (especially in terms of eigensolvers, perhaps a way to use GPU acceleration?)
* Utilities in creating waveguide index structures and analyzing mode results (in separate related packages)
* Perhaps add support for vector field and isotropic media (provided a simple code can be written to implement these features)?
* Implement various boundary conditions
* Benchmarks and performance overview/guidelines

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support both 1D and 2D waveguides
* We aim to support both uniformly and non-uniformly sampled grids
* We aim to minimize the amount of code and function repetition when implementing the above features
