# Example: 1D Waveguide Modes

Let's calculate the modes of a simple 1D slab waveguide. We'll assume
a core index value of 3.1 and cladding index value of 3.0, waveguide
width of 2 $\mu$m and wavelength of 1.55 $\mu$m. First
we'll import the package and define the relevant variables:

```@example 1d
import FDMModes

lambda=1.55
n_core, n_clad=3.1, 3.0
dx=0.01
x=-5:dx:5
waveguide_width=2
index=[abs(x)<=waveguide_width/2 ? n_core : n_clad for x=x]
nothing # hide
```

We plot the index structure using the `Plots` package:

```@example 1d
import Plots: plot, scatter
import Plots: savefig # hide
plot(x, index,
	xlabel="x",
	ylabel="Refractive Index",
	legend=false,
)
savefig("slab_wvg_index.svg"); nothing # hide
```

![Plot of the refractive index for a 1D slab waveguide](slab_wvg_index.svg)

Now that we've made the waveguide index structure we can calculate the
waveguide eigenmodes: 

```@example 1d
neffs, modes=FDMModes.waveguidemodes(index, lambda, dx)
nothing # hide
```

We now plot the results:

```@example 1d
plot(
	scatter(neffs, ylabel="Modal Effective Index", legend=false),
	plot(x, modes,
		xlabel="x",
		ylabel="Modal Field",
		legend=false,
	)
)
savefig("slab_wvg_sol.svg"); nothing # hide
```

![Plot of the waveguide eigenmode solutions](slab_wvg_sol.svg)

The mode-solver has found 2 waveguide modes with mode profiles that
qualitatively look as expected.

Now on to something a bit more interesting. Let's consider adding a
lower index (3.09) perturbation to the waveguide core:

```@example 1d
n_core, n_clad, n_pert=3.1, 3.0, 3.09
dx=0.01
x=-5:dx:5
waveguide_width=2
pert_width=1
index=[abs(x)<=waveguide_width/2 ? (abs(x)<=pert_width/2 ? n_pert : n_core) : n_clad for x=x]
neffs, modes=FDMModes.waveguidemodes(index, lambda, dx)
nothing # hide
```

We plot the waveguide and the mode solutions:

```@example 1d
plot(
	plot(x, index,
		xlabel="x",
		ylabel="Refractive Index",
		legend=false,
	),
	scatter(neffs, ylabel="Modal Effective Index", legend=false),
	plot(x, modes,
		xlabel="x",
		ylabel="Modal Field",
		legend=false,
	)
)
savefig("slab_wvg_pert.svg"); nothing # hide
```

![Plot of the perturbed slab waveguide and its eigenmode solutions](slab_wvg_pert.svg)

Once again we find two confined modes but the field profiles are now
distorted relative to the simple slab waveguide. In particular,
we can see that the fundamental mode is now "shorter but wider".
