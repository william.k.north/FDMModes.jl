# FDMModes.jl Documentation

## About

`FDMModes` seeks to be a dielectric waveguide mode solving package that
is simple to understand, use, and maintain. It implements scalar
mode solvers on both uniform and non-uniform grids in 1D and 2D.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMModes.jl
```

## Features

* Scalar mode solving
* 1D and 2D waveguides
* Uniform and non-uniform finite grids

## Related Packages

* [FDMGridWaveguides.jl](https://gitlab.com/pawelstrzebonski/FDMGridWaveguides.jl) for functions to help generating waveguide index structures used as inputs to `FDMModes`
* [FDMModeAnalysis.jl](https://gitlab.com/pawelstrzebonski/FDMModeAnalysis.jl) for functions to help analyzing the results of `FDMModes`
* [WaveguideDesigner.jl](https://gitlab.com/pawelstrzebonski/WaveguideDesigner.jl) for functions to help design waveguide index structures for a given modal field
