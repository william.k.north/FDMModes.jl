# laplacian.jl

## Description

These unit tests compare the non-uniform grid Laplacian operator functions
to the uniform grid version in the case in which all of the grid spacing
is equal, to ensure that the two are equal in this special case. They
also ensure that the functions that take the `x`/`y` values are equivalent
to those that take `dx`/`dy` as arguments.
