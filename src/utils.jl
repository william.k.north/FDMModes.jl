"""
    estimate_num_modes(dx[, dy], n, wavelength)

Estimate the upper limit on the number of modes that could be supported
by the waveguide of index structure `n` with grid spacing `dx` (and `dy`
for 2D). Estimate is based solely on the ratio of the optical dimensions
of the waveguide relative to the free-space `wavelength`.
"""
function estimate_num_modes(dx, n::AbstractVector, wavelength::Number)
    Lopt = sum(dx .* n)
    Nmodes = round(Int, ceil(2 * Lopt / wavelength))
end
function estimate_num_modes(dx, dy, n::AbstractMatrix, wavelength::Number)
    Loptx = maximum([sum(dx .* n) for n in eachcol(n)])
    Lopty = maximum([sum(dy .* n) for n in eachrow(n)])
    Nmodesx = round(Int, ceil(2 * Loptx / wavelength))
    Nmodesy = round(Int, ceil(2 * Lopty / wavelength))
    Nmodes = Nmodesx * Nmodesy
end
