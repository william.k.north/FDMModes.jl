import LinearAlgebra, SparseArrays

"""
    fd2coeffs(x1, x2, x3) -> (dl, d, du)

Calculate the finite difference coefficients given left, center, and 
right sample points `x1`, `x2`, and `x3`. Returns the lower, center, and
upper diagonals `dl`, `d`, and `du` for the Laplacian matrix operator.
"""
fd2coeffs(x1, x2, x3) = @. [
    2 / ((x2 - x1) * (x3 - x1)),
    -2 / ((x2 - x1) * (x3 - x2)),
    2 / ((x3 - x1) * (x3 - x2)),
]

"""
    fd2coeffs(dxl, dxr) -> (dl, d, du)

Calculate the finite difference coefficients given left and 
right sample spacing `dxl` and `dxr`. Returns the lower, center, and
upper diagonals `dl`, `d`, and `du` for the Laplacian matrix operator.
"""
fd2coeffs(dxl, dxr) =
    @. [2 / (dxl * (dxl + dxr)), -2 / (dxl * dxr), 2 / ((dxl + dxr) * dxr)]

"""
    laplacian_x(x[, y])

Calculate the finite difference Laplacian matrix operator given an array
of grid samples `x` (and `y` for 2D).
"""
function laplacian_x(x::AbstractVector)
    xpadded = [2 * x[1] - x[2]; x; 2 * x[end] - x[end-1]]
    vl, vd, vu = fd2coeffs(xpadded[1:end-2], xpadded[2:end-1], xpadded[3:end])
    LinearAlgebra.Tridiagonal(vl[2:end], vd, vu[1:end-1])
end
function laplacian_x(x::AbstractVector, y::AbstractVector)
    Lx, Ly = laplacian_x(x), laplacian_x(y)
    #TODO: Issues with kron() with non-sparse() special arrays
    #Ideally sparse() shouldn't be needed and sparseness would be
    #conserved during kron()
    #kron(Diagonal, Tridiagonal)+kron(Tridiagonal, Diagonal)=SparseMatrixCSC
    #Possible fix: https://github.com/JuliaLang/julia/pull/24980
    LinearAlgebra.kron(
        SparseArrays.spdiagm(0 => ones(length(y))),
        SparseArrays.sparse(Lx),
    ) +
    LinearAlgebra.kron(SparseArrays.sparse(Ly), SparseArrays.spdiagm(0 => ones(length(x))))
end

"""
    laplacian_dx(dx[, dy])

Calculate the finite difference Laplacian matrix operator given the grid
non-uniform spacing `dx` (and `dy` for 2D).
"""
function laplacian_dx(dx::AbstractVector)
    dxpadded = [dx[1]; dx; dx[end]]
    vl, vd, vu = fd2coeffs(dxpadded[1:end-1], dxpadded[2:end])
    LinearAlgebra.Tridiagonal(vl[2:end], vd, vu[1:end-1])
end
function laplacian_dx(dx::AbstractVector, dy::AbstractVector)
    Lx, Ly = laplacian_dx(dx), laplacian_dx(dy)
    #TODO: Issues with kron() with non-sparse() special arrays
    #Ideally sparse() shouldn't be needed and sparseness would be
    #conserved during kron()
    #kron(Diagonal, Tridiagonal)+kron(Tridiagonal, Diagonal)=SparseMatrixCSC
    #Possible fix: https://github.com/JuliaLang/julia/pull/24980
    LinearAlgebra.kron(
        SparseArrays.spdiagm(0 => ones(length(dy) + 1)),
        SparseArrays.sparse(Lx),
    ) + LinearAlgebra.kron(
        SparseArrays.sparse(Ly),
        SparseArrays.spdiagm(0 => ones(length(dx) + 1)),
    )
end

"""
    laplacian_dx(dx, Nx[, dy, Ny])

Calculate the finite difference Laplacian matrix operator given the grid
uniform spacing `dx` (and `dy` for 2D) for a grid with `Nx` many points
(and `Ny` for 2D).
"""
function laplacian_dx(dx::Number, N::Integer)
    LinearAlgebra.SymTridiagonal(fill(-2 / dx^2, N), fill(1 / dx^2, N - 1))
end
function laplacian_dx(dx::Number, Nx::Integer, dy::Number, Ny::Integer)
    Lx, Ly = laplacian_dx(dx, Nx), laplacian_dx(dy, Ny)
    #TODO: Issues with kron() with non-sparse() special arrays
    #Ideally sparse() shouldn't be needed and sparseness would be
    #conserved during kron()
    #kron(Diagonal, Tridiagonal)+kron(Tridiagonal, Diagonal)=SparseMatrixCSC
    #Possible fix: https://github.com/JuliaLang/julia/pull/24980
    LinearAlgebra.kron(SparseArrays.spdiagm(0 => ones(Ny)), SparseArrays.sparse(Lx)) +
    LinearAlgebra.kron(SparseArrays.sparse(Ly), SparseArrays.spdiagm(0 => ones(Nx)))
end
